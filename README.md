<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

Database : backend_azka_mnc

Link Download Database : https://drive.google.com/file/d/1dZi6tAgJ9xu4p7wr4pcHizIVQrDCe0lm/view?usp=sharing

Link Download Env : https://drive.google.com/file/d/1f4GIHRXSlfzlDOuQH6uOs-v49Gx3tLzE/view?usp=sharing


Collection POSTMAN

GET DATA INVOICE ENDPOINT
http://127.0.0.1:8000/api/get-invoice/2022-02-09

POST DATA INVOICE ENDPOINT
http://127.0.0.1:8000/api/post-invoice

DELETE DATA INVOICE ENDPOINT
http://127.0.0.1:8000/api/delete-invoice/9

PUT DATA INVOICE ENDPOINT
http://127.0.0.1:8000/api/put-invoice/7

Link Collection JSON API : https://drive.google.com/file/d/12Cynbd1kmJKoNFRdsFTD3Lha0I8lwLdW/view?usp=sharing

