<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = "invoice_table";
 
    protected $fillable = ['no','date','customer_name','salesperson_name','payment_type','notes','product_id','created_at','updated_at'];
}
