<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Invoice;

use Illuminate\Support\Facades\Validator; 

class ApiInvoiceController extends Controller
{
    public function getInvoice($date){
        
        $invoice = Invoice::orderby('invoice_table.created_at','desc')
        ->select('invoice_table.invoice_no','invoice_table.date','invoice_table.customer_name',
        'invoice_table.salesperson_name', 'invoice_table.payment_type',
        'invoice_table.notes','product_table.item_name','product_table.quantity','product_table.total_cost_of_goods_sold',
        'product_table.total_price_sold')
        ->where('invoice_table.date',$date)
        ->join('product_table','product_table.id','invoice_table.product_id')
        ->first();

        $price_sold = $invoice['total_price_sold'];
        $cog = $invoice['total_cost_of_goods_sold'];

        $profit = number_format($price_sold - $cog);
        
        return response([
            'status' => 200,
            'Date'   => $date,
            'Total Profit' => $profit,
            'invoice' => $invoice
        ], 200);

    }
    public function postInvoice (Request $request){

        $validasi = Validator::make($request->all(), [
            'invoice_no'      => 'required|min:1|numeric|unique:invoice_table,invoice_no',
            'date'            => 'required|date',
            'customerName'    => 'required|min:2',
            'salespersonName' => 'required|min:2',
            'payment_type'    => 'required',
            'notes'           => 'required|min:5',
            'productId'       => 'required'
        ]);

        if($validasi->fails()){
            return response()->json( $validasi->errors() );
        }else{
            $invoice = new Invoice;
            $invoice->invoice_no = $request->invoice_no;
            $invoice->date = $request->date;
            $invoice->customer_name = $request->customerName;
            $invoice->salesperson_name = $request->salespersonName;
            $invoice->payment_type = $request->payment_type;
            $invoice->notes = $request->notes;
            $invoice->product_id = $request->productId;
        
            if($invoice->save()){
                return response([
                    'status' => 200,
                    'message' => 'Create Invoice Success',
                    'data'   => $invoice
                ], 200);
            }else{
                return response([
                    'status' => 200,
                    'message' => 'Gagal Create Invoice'
                ], 400);
            }
        }
    }
    public function deleteInvoice($invoice_no){
        $del = Invoice::where('invoice_no',$invoice_no);
        if($del->delete()){
            return response([
                'status'     => 200,
                'invoice_no' => $invoice_no,
                'message'    => 'Deleted Invoice Success'
            ], 200);
        }else{
            return response([
                'status' => 400,
                'message' => 'Gagal Deleted Invoice'
            ],400);
        }
    }
    public function putInvoice($invoice_no, Request $request){
        $validasi = Validator::make($request->all(), [
            'date'            => 'required|date',
            'customerName'    => 'required|min:2',
            'salespersonName' => 'required|min:2',
            'payment_type'    => 'required',
            'notes'           => 'required|min:5',
            'productId'       => 'required'
        ]);

        if($validasi->fails()){
            return response()->json( $validasi->errors() );
        }else{
            $invoice = Invoice::where('invoice_no',$invoice_no);

            $invoice->update(
                [
                    'date'              => $request->date,
                    'customer_name'     => $request->customerName,
                    'salesperson_name'  => $request->salespersonName,
                    'payment_type'      => $request->payment_type,
                    'notes'             => $request->notes,
                    'product_id'        => $request->productId
                ]
            );
            
            return response([
                'status' => 200,
                'invoice_no'   => $invoice_no,
                'message' => 'Update Invoice Success'
            ], 200);
        }
    }
}
