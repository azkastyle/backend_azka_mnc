public class azkaFunctionMaxArray {

    public static void main(String[] args) {
        double[] decMax = {23, 76, 45, 20, 70, 65, 15, 54};

        double maxx = decMax[0];

        for (int i = 0; i < decMax.length; i++) {
            if (maxx < decMax[i]) {
                maxx = decMax[i];
            }
        }
        System.out.println(maxx);
    }
}