# Azka Willdan Mukholladun
# MNC
# Scrapping 

import requests
from bs4 import BeautifulSoup

url_okezone = 'https://bola.okezone.com/read/2022/08/05/47/2642706/juventus-butuh-paul-pogba-ini-harapan-massimiliano-allegri'

print("=============== AZKA MNC MEDIA =============================================================================================================")
print("Scrapping Artikel Okezone "+ url_okezone)
print("=============== LIST JUDUL BERITA TERKAIT  =================================================================================================")

laman_ss = requests.get(url_okezone)

soup = BeautifulSoup(laman_ss.content, 'html.parser')

kontainer_berita = soup.find_all('div', class_='swiper-wrapper widget-slider-next')

judul_list = []

for berita in kontainer_berita:
    for judul in berita.find_all('h3'):
        judul_string = judul.get_text()
        judul_rapi = ' '.join(str(judul_string).strip().split()) 
        print(judul_rapi)
        judul_list.append(judul_rapi)