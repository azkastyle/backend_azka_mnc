// AZKA WILLDAN MUKHOLLADUN
// MNC MEDIA

import java.util.*;

class AzkaFunctionCombination{

	static void

	Recurrence(int l, int t, ArrayList<Integer> sub_vector, boolean[] vis, ArrayList<ArrayList<Integer>> output,int last)
	{
		if (l == 0 && t == 0)
		{
			output.add(new ArrayList<>(sub_vector));
			return;
		}

		if (l <= 0 || t <= 0)
			return;

		for(int i = last; i <= 9; i++)
		{
			
			if (!vis[i])
			{
				
				vis[i] = true;

				sub_vector.add(i);

				Recurrence(l - 1, t - i, sub_vector, vis,
						output, i + 1);

				sub_vector.remove(sub_vector.size() - 1);

				vis[i] = false;
			}
		}
	}

	static void combinationSum(int l, int t)
	{
		
		if (l * 9 < t)
		{
			System.out.print("Impossible");
			return;
		}

		boolean[] vis = new boolean[10];

		ArrayList<Integer> sub_vector = new ArrayList<>();
		ArrayList<ArrayList<Integer>> output = new ArrayList<>();

		Recurrence(l, t, sub_vector, vis, output, 1);

		for(int i = 0; i < output.size(); i++)
		{
			for(Integer x : output.get(i))
				System.out.print(x + " ");
				
			System.out.println();
		}
		return;
	}

	public static void main(String[] args)
	{
		int l = 3, t = 8;
		
		combinationSum(l, t);
	}
}

